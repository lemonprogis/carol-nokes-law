import * as React from 'react';
import {Route, Switch} from "react-router";
import {BrowserRouter} from "react-router-dom";
import HomeContainer from "./containers/HomeContainer"

export default  () => {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path={'/'} exact={true} component={HomeContainer}/>
                    <Route path={'/attorney-ad-litem'} exact={true} component={HomeContainer}/>
                    <Route path={'/mediation'} exact={true} component={HomeContainer}/>
                    <Route path={'/directions'} exact={true} component={HomeContainer}/>
                    <Route path={'/links'} exact={true} component={HomeContainer}/>
                </Switch>
            </BrowserRouter>
        )



}