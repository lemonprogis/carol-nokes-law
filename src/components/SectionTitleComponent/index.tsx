import {Header} from "semantic-ui-react";
import * as React from "react";

export const SectionTitleComponent = ({title}: {title: string}) => {
  return (
      <Header as={"h2"} textAlign={"center"}>{title || ''}</Header>
  )
};