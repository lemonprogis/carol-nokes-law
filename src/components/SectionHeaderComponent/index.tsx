import {Header} from "semantic-ui-react";
import * as React from "react";

export const SectionHeaderComponent = ({headerSection}: {headerSection: string}) => {
    return (
        <Header as={'h1'} size={"medium"} className={"large-quote"}>
            {headerSection || ''}
        </Header>
    )
};