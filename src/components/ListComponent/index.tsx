import * as React from "react";
import {List, ListItemProps, SemanticShorthandItem} from "semantic-ui-react";

export const ListComponent = ({header, items}: {header:string, items: SemanticShorthandItem<ListItemProps>[]}) => {
    if(header === '' || header === null) {
        return (
            <div>
                <p>
                    <List items={items} bulleted={true} />
                </p>
            </div>
        )
    }
    else {
        return (
            <div>
                <p>
                    {header}
                </p>
                <p>
                    <List items={items} bulleted={true} />
                </p>
            </div>
        )
    }
};