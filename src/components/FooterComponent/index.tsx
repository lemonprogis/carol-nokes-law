import * as React from 'react';
import {Menu} from "semantic-ui-react";
import moment from 'moment';
import './index.css'

export const FooterComponent = ({phone, email}: {phone: string, email: string}) => {
    let year = moment().year();
    return (
        <Menu id={'footer-menu'} widths={3}>
            <Menu.Item />
            <Menu.Item className={'footer-item'}>
                    <span>
                        &copy;2016 - {year} Carol D. Nokes - domestic & probate Attorney ad Litem, domestic mediator
                        <br />
                        <a href={`tel:${phone}`} target={'_blank'}>Phone: {phone}</a>&nbsp;&nbsp;
                        <a href={`tel:${email}`} target={'_blank'}>Email: {email}</a>
                    </span>
            </Menu.Item>
            <Menu.Item />
        </Menu>
    )
};
