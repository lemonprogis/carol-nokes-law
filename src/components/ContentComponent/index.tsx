import * as React from "react";
import {Section} from "../../models/application";

export const ContentComponent = ({sections}: {sections: Section[]}) => {
    const content = sections.map( (section: Section) => {
        if(section.header === "") {
            return (
                <div>
                    <span>{section.content}</span>
                </div>
            )
        }
        else {
            return (
                <div>
                    <span>{section.header}</span>
                    <span>{section.content}</span>
                </div>
            )
        }
    });

    return content;
};