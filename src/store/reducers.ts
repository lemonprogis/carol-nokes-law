import * as types from './types';
import {GlobalStore} from "../models/application";

const initialState: GlobalStore = {
    apiKey: 'AIzaSyA_T1-jMqnGOQ_M2-PiJMZi3N3vIvrNYLY',
    appInfo: {
        phone: '1-501-223-9620',
        email: 'nokeslaw@aol.com'
    },
    homeSection: {
        title: "",
        headerSection: "",
        sections: []
    },
    attorneyAdLitemSection: {
        title: "",
        domestic: {
            headerSection: "",
            sections: []
        },
        probate: {
            headerSection: "",
            sections: []
        }
    },
    mediationSection: {
        title: "",
        headerSection: "",
        sections: []
    },
    directionSection: {
        title: "",
        headerSection: "",
        sections: []
    },
    linkSection: {
        title: "",
        headerSection: "",
        links: []
    }
};


export const reducer = (state = initialState, action:any) => {
    switch(action.type) {
        case types.GET_HOME_SECTION:
            return {
                ...state,
                homeSection: action.payload
            };
        case types.GET_ATTORNEY_AD_LITEM_SECTION:
            return {
                ...state,
                attorneyAdLitemSection: action.payload
            };
        case types.GET_MEDIATION_SECTION:
            return {
                ...state,
                mediationSection: action.payload
            };
        case types.GET_DIRECTION_SECTION:
            return {
                ...state,
                directionSection: action.payload
            };
        case types.GET_LINK_SECTION:
            return {
                ...state,
                linkSection: action.payload
            };
        default:
            return state;
    }
};


