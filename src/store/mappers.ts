import {GlobalStore} from "../models/application";
import {
    getAttorneyAdLitemContent,
    getDirectionsContent,
    getHomeContent, getLinksContent,
    getMediationContent
} from "../services/app-service";

export const mapStateToProps = (state: GlobalStore) => {
    return {
        ...state
    }
};

export const mapDispatchToProps = (dispatch: any) => {
    return {
        getHomeSection: () => getHomeContent(dispatch),
        getAttorneyAdLitemSection: () => getAttorneyAdLitemContent(dispatch),
        getMediationSection: () => getMediationContent(dispatch),
        getDirectionSection: () => getDirectionsContent(dispatch),
        getLinkSection: () => getLinksContent(dispatch)
    }
};