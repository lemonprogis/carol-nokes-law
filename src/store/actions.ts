import {AttorneyAdLitem, Directions, Home, Links, Mediation} from "../models/application";



export const resolvedGetHomeSection = (data: Home) => {
    return {
        type: 'GET_HOME_SECTION',
        payload: data
    }
};

export const resolvedGetAttorneyAdLitemSection = (data: AttorneyAdLitem) => {
  return {
      type: 'GET_ATTORNEY_AD_LITEM_SECTION',
      payload: data
  }
};

export const resolvedGetMediationSection = (data: Mediation) => {
    return {
        type: 'GET_MEDIATION_SECTION',
        payload: data
    }
};

export const resolvedGetDirectionSection = (data: Directions) => {
    return {
        type: 'GET_DIRECTION_SECTION',
        payload: data
    }
};

export const resolvedGetLinkSection = (data: Links) => {
    return {
        type: 'GET_LINK_SECTION',
        payload: data
    }
};
