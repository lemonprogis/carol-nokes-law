export interface AppInfo {
    phone: string;
    email: string;
}

export interface GlobalStore {
    apiKey: string;
    appInfo: AppInfo;
    homeSection: Home;
    attorneyAdLitemSection: AttorneyAdLitem;
    mediationSection: Mediation;
    directionSection: Directions;
    linkSection: Links;
}

export interface Section {
    header: string;
    content: string[];
    asList?: boolean;
}

export interface Mediation {
    title: string;
    headerSection: string;
    sections: Section[];
}

export interface Home {
    title: string;
    headerSection: string;
    sections: Section[];
}

export interface Directions {
    title: string;
    headerSection: string;
    sections: Section[];
}

export interface Domestic {
    headerSection: string;
    sections: Section[];
}


export interface Probate {
    headerSection: string;
    sections: Section[];
}

export interface AttorneyAdLitem {
    title: string;
    domestic: Domestic;
    probate: Probate;
}

export interface Link {
    value: string;
    content: string;
}

export interface Links {
    title: string;
    headerSection: string;
    links: Links[];
}