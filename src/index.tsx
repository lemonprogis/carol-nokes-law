import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {reducer} from "./store/reducers";
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import AppRoutes from "./routes";

import 'react-app-polyfill/ie11';
import 'semantic-ui-css/semantic.min.css';
import './index.css';

import WebFont from 'webfontloader';

WebFont.load({
    google: {
        families: ['Open Sans', 'sans-serif']
    }
});

const store = createStore(reducer, applyMiddleware(thunk));

ReactDOM.render(
        <Provider store={store}>
            <AppRoutes />
        </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
