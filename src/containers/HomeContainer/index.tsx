import * as React from 'react';
import {connect} from "react-redux";
import {mapDispatchToProps, mapStateToProps} from "../../store/mappers";

import './index.css'
import {RouteComponentProps} from "react-router";
import {Container, Embed, Grid, Header, HtmlIframeProps, Image, List} from "semantic-ui-react";
import {AppInfo, AttorneyAdLitem, Directions, Home, Link, Links, Mediation, Section} from "../../models/application";
import {SectionHeaderComponent} from "../../components/SectionHeaderComponent";
import {SectionTitleComponent} from "../../components/SectionTitleComponent";
import {FooterComponent} from "../../components/FooterComponent";

interface PageProps extends RouteComponentProps {
    appInfo: AppInfo;
    homeSection: Home;
    attorneyAdLitemSection: AttorneyAdLitem;
    mediationSection: Mediation;
    directionSection: Directions;
    linkSection: Links;
    getHomeSection: () => void;
    getAttorneyAdLitemSection: () => void;
    getMediationSection: () => void;
    getDirectionSection: () => void;
    getLinkSection: () => void;
}

interface PageState {
}

class HomeContainer extends React.Component<PageProps, PageState> {
    constructor(props: PageProps) {
        super(props);
    }

    componentDidMount(): void {
        this.props.getHomeSection();
        this.props.getAttorneyAdLitemSection();
        this.props.getMediationSection();
        this.props.getDirectionSection();
        this.props.getLinkSection();
    }

    renderSection = (sections: any[]) => {
        const section = sections.map( (section: Section, index: number) => {
            const contents = section.content.map( (content: string, index: number) => {
                return section.asList ?
                    (<List.Item key={index} className={"content-list-item"}>{content}</List.Item>) :
                    (<p key={index}>{content}</p>)
            });

            const listContents = <List items={contents} bulleted={true} className={"content-list"} />;

            return (
                <div key={index}>
                    <p className={"content-header"}><b>{section.header}</b></p>
                    {section.asList ? listContents: contents}
                </div>
            )
        });

        return section;
    };

    renderMap = () => {
        const iframe: HtmlIframeProps = {
            allowFullScreen: true,
            style: {
                padding: 10,
                border: 0
            },
            source: "",
            src: "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13113.770848525553!2d-92.27574577672121!3d34." +
                "74443913710414!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xafdea2faa695ba24!2s" +
                "Metropolitan+National+Bank!5e0!3m2!1sen!2sus!4v1455257405437"
        };
        return (
            <div>
            <Embed iframe={iframe} active={true} />
            </div>
        )
    };

    renderLinkSection = (links: any[]) => {
        const linkItems = links.map( (link: Link, index: number) => {
            return (<List.Item key={index} className={"content-list-item"}>
                <a href={link.value}>{link.content}</a></List.Item>)
        });

        return <List items={linkItems} className={"content-list"} />;
    };

    public render() {
        return (
            <Container>
                <Grid stackable={true} columns={16}>
                    <Grid.Row>
                        <Grid.Column width={6}>
                            <Image src={'/images/header.png'} />
                        </Grid.Column>
                        <Grid.Column width={6} floated={"right"}>
                            <Header
                                as={'h2'}
                                content={'Contact:'}
                                subheader={`Telephone: ${this.props.appInfo.phone} Email: ${this.props.appInfo.email}`}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <SectionHeaderComponent headerSection={this.props.homeSection.headerSection} />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={10}>
                            {this.renderSection(this.props.homeSection.sections)}
                        </Grid.Column>
                        <Grid.Column width={6} floated={"right"}>
                                <Image src={'/images/carol-nokes-law.jpg'} rounded={true} size={"medium"}/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <SectionTitleComponent title={this.props.attorneyAdLitemSection.title} />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <SectionHeaderComponent
                                headerSection={this.props.attorneyAdLitemSection.domestic.headerSection} />
                            {this.renderSection(this.props.attorneyAdLitemSection.domestic.sections)}
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <SectionHeaderComponent
                                headerSection={this.props.attorneyAdLitemSection.probate.headerSection} />
                            {this.renderSection(this.props.attorneyAdLitemSection.probate.sections)}
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <SectionTitleComponent title={this.props.mediationSection.title} />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <SectionHeaderComponent
                                headerSection={this.props.mediationSection.headerSection} />
                            {this.renderSection(this.props.mediationSection.sections)}
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <SectionTitleComponent title={this.props.directionSection.title} />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={8}>
                            {this.renderSection(this.props.directionSection.sections)}
                        </Grid.Column>
                        <Grid.Column width={8}>
                            {this.renderMap()}
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <SectionTitleComponent title={this.props.linkSection.title} />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={8}>
                            {this.renderLinkSection(this.props.linkSection.links)}
                            <br />
                            <br />
                            <br />
                            <br />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={16} verticalAlign={'bottom'}>
                            <FooterComponent phone={this.props.appInfo.phone} email={this.props.appInfo.email}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);