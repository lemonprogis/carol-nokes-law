import axios from "axios";
import {Dispatch} from "redux";
import {
    resolvedGetAttorneyAdLitemSection,
    resolvedGetDirectionSection,
    resolvedGetHomeSection,
    resolvedGetLinkSection,
    resolvedGetMediationSection
} from "../store/actions";


export const getHomeContent = (dispatch: Dispatch): void => {
    const url = `/data/home.json`;
    axios.get(url)
        .then(({ status, data} ) => {
            if (status === 200) {
                dispatch(resolvedGetHomeSection(data));
            } else {
                console.error("error getting data.");
            }
        });
};

export const getAttorneyAdLitemContent = (dispatch: Dispatch): void => {
    const url = `/data/attorney-ad-litem.json`;
    axios.get(url)
        .then(({ status, data} ) => {
            if (status === 200) {
                dispatch(resolvedGetAttorneyAdLitemSection(data));
            } else {
                console.error("error getting data.");
            }
        });
};

export const getMediationContent = (dispatch: Dispatch): void => {
    const url = `/data/mediation.json`;
    axios.get(url)
        .then(({ status, data} ) => {
            if (status === 200) {
                dispatch(resolvedGetMediationSection(data));
            } else {

            }
        });
};

export const getDirectionsContent = (dispatch: Dispatch): void => {
    const url = `/data/directions.json`;
    axios.get(url)
        .then(({ status, data} ) => {
            if (status === 200) {
                dispatch(resolvedGetDirectionSection(data));
            } else {
                console.error("error getting data.");
            }
        });
};

export const getLinksContent = (dispatch: Dispatch): void => {
    const url = `/data/links.json`;
    axios.get(url)
        .then(({ status, data} ) => {
            if (status === 200) {
                dispatch(resolvedGetLinkSection(data));
            } else {
                console.error("error getting data.");
            }
        });
};